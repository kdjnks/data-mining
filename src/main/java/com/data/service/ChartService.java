package com.data.service;

import com.data.models.Pair;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;

public interface ChartService<T> {

    void showChartForCollection(List<T> collection);

    default XYSeriesCollection getCollection(String title, List<Pair<String, Long>> pairList) {
        BiFunction<XYSeriesCollection, XYSeries, XYSeriesCollection> xySeriesXYSeriesCollectionBiFunction = (xySeriesCollection, series) -> {
            xySeriesCollection.addSeries(series);
            return xySeriesCollection;
        };

        BinaryOperator<XYSeriesCollection> binaryOperator = (collection1, collection2) -> {
            List<XYSeries> series = collection1.getSeries();
            series.forEach(collection2::addSeries);
            return collection1;
        };

        return IntStream.range(0, 19)
                .mapToObj(index -> {
                    Pair<String, Long> stringLongPair = pairList.get(index);
                    XYSeries xySeries = new XYSeries(stringLongPair.getLeft());
                    xySeries.add(index, stringLongPair.getRight());
                    return xySeries;
        }).reduce(new XYSeriesCollection(), xySeriesXYSeriesCollectionBiFunction, binaryOperator);
    }

    default XYSeriesCollection getCollectionWithAverage(String title, Map<Integer, Long> collected) {
        BiFunction<XYSeries, Map.Entry<Integer, Long>, XYSeries> xySeriesBiFunction = (xySeries, entry) -> {
            xySeries.add(entry.getKey(), entry.getValue());
            return xySeries;
        };

        BinaryOperator<XYSeries> binaryOperator = (collection1, collection2) -> {
            List<XYDataItem> series = collection1.getItems();
            series.forEach(collection2::add);
            return collection1;
        };

        XYSeries finalCollection = collected.entrySet().stream().reduce(new XYSeries(title), xySeriesBiFunction, binaryOperator);

        XYSeries average = new XYSeries("Average for " + title);
        double averageNumber = (Collections.max(collected.keySet()) + Collections.min(collected.keySet())) / 2.0;

        average.add(averageNumber, 0);
        average.add(averageNumber, Collections.max(collected.values()));

        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
        xySeriesCollection.addSeries(finalCollection);
        xySeriesCollection.addSeries(average);

        return xySeriesCollection;
    }
}
