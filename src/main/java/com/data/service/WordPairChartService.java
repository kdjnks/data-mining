package com.data.service;

import com.data.graphs.Chart;
import com.data.models.Pair;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WordPairChartService implements ChartService<Pair<String, Long>> {

    private String title;

    public WordPairChartService(String title) {
        this.title = title;
    }

    @Override
    public void showChartForCollection(List<Pair<String, Long>> collection) {
        Map<Integer, Long> collected = collection.stream()
                .collect(Collectors.groupingBy(
                        (element) -> element.getLeft().length(),
                        Collectors.summingLong(Pair::getRight))
                );

        collected.forEach((k,v) -> System.out.println(k+":"+v));

        new Chart(title,"Words","Frequency", getCollectionWithAverage(title, collected));
    }

    public void showAverage(List<Pair<String, Long>> collection) {
        new Chart(title,"Words","Frequency", getCollection(title, collection));
    }
}
