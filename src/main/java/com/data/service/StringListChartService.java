package com.data.service;

import com.data.graphs.Chart;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringListChartService implements ChartService<String> {

    private String title;

    public StringListChartService(String title) {
        this.title = title;
    }

    @Override
    public void showChartForCollection(List<String> collection) {
        Map<Integer, Long> collect = collection.stream()
                .collect(
                        Collectors.groupingBy(
                            String::length,
                            Collectors.counting()));

        new Chart(title,"Words","Frequency", getCollectionWithAverage(title, collect));
    }
}
