package com.data;

import com.data.enums.EmailType;
import com.data.models.Pair;
import com.data.service.StringListChartService;
import com.data.service.WordPairChartService;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Application {
    public static void main(String[] args) throws IOException {
        new Application();
    }

    public Application() {
        run();
    }

    private void run() {
        List<String> fileList = new ArrayList<>();

        try {
            fileList = Files
                    .lines(Paths.get(ClassLoader.getSystemResource("sms-spam-corpus.csv").toURI()), StandardCharsets.ISO_8859_1)
                    .skip(1)
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        Map<EmailType, List<String>> collect1 = fileList.stream()
                .map(this::pair)
                .collect(
                        Collectors.groupingBy(
                                Pair::getLeft,
                                Collectors.mapping(Pair::getRight, Collectors.toList())));

        collect1.forEach((k,v) -> new StringListChartService(k.name() + " / MESSAGE").showChartForCollection(v));

        Map<EmailType, List<Pair<String, Long>>> collect = fileList.stream()
                .map(this::pair)
                .map(this::analyze)
                .collect(
                        Collectors.groupingBy(
                                Pair::getLeft,
                                Collectors.flatMapping(pair -> pair.getRight().stream(), Collectors.toList())))
                .entrySet()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey,
                                Collectors.flatMapping(entr -> this.countWords(entr).stream(), Collectors.toList())
                        )
                );

        collect.forEach((k,v) -> {
            try(Writer fileWriter = Files.newBufferedWriter(Path.of(String.format("output/%s.txt", k)))) {
                v.forEach(pair -> {
                    try {
                        fileWriter.write(pair.getLeft() + ":" + pair.getRight() + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                fileWriter.flush();
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            new WordPairChartService(k.name() + " / WORDS").showChartForCollection(v);
            new WordPairChartService(k.name() + " / FREQUENT").showAverage(v.stream().sorted(Comparator.comparing(Pair::getRight, Comparator.reverseOrder())).collect(Collectors.toList()).subList(0, 19));
        });

        //EventQueue.invokeLater(() -> HistogramPanel.createAndShowGUI(collect));
    }

    private List<Pair<String, Long>> countWords(Map.Entry<EmailType, List<String>> entry) {
        return entry.getValue().stream()
                .collect(
                        Collectors.groupingBy(
                            Function.identity(),
                            Collectors.counting())
                )
                .entrySet()
                .stream()
                .map(entry1 -> new Pair<>(entry1.getKey(), entry1.getValue()))
                .collect(Collectors.toList());

    }

    private Pair<EmailType, String> pair(String line) {
        String[] split = line.split(",", 2); //emailType and message

        return new Pair<>(EmailType.valueOf(split[0].toUpperCase()), split[1].toLowerCase().substring(0, split[1].length() - 3));
    }

    private Pair<EmailType, List<String>> analyze(Pair<EmailType, String> pair) {
        List<String> collected = new ArrayList<>();

        try {
            collected = analyze(pair.getRight(), new StopAnalyzer(EnglishAnalyzer.getDefaultStopSet()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Pair<>(pair.getLeft(), collected);
    }

    private List<String> analyze(String text, Analyzer analyzer) throws IOException{
        List<String> result = new ArrayList<>();
        TokenStream tokenStream = analyzer.tokenStream(null, new StringReader(text));
        CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream = new PorterStemFilter(tokenStream);
        tokenStream.reset();

        while(tokenStream.incrementToken()) {
            result.add(attr.toString());
        }

        return result;
    }
}